# MegascansMat_Fun

## About

Example/test for Layered Material Blending for Megascans meshes to achieve texture size reduction, tileable texture reuse, and asset quality preservation. Emphasized game and VRAM optimization to enhance performance.

## Description
I initiated this project approximately a year ago with the objective of optimizing space and VRAM usage for Megascans meshes and materials. This was accomplished through the development of layered materials incorporating blending functions. The system leverages four primary blending techniques: NoiseTexture, VertexColor, WorldNormal vector, and World Z offset. The utilization of layered materials from Unreal Engine 5 enables seamless blending of these techniques.

## Installation
The project is based on Unreal Engine 5.2.1, utilizing pure blueprints. While there are remnants of the Unreal Lyra plugin, I have removed it, causing a few procedural mesh blueprints to be non-functional. Installation is not required if you already have Unreal Engine.

## License
The project is an open-source, and does not rely on any marketplace assets, with the exception of freely available Megascans textures and meshes.

## Project status
Development for this project has been put on hold until there is a requirement for a similar system in my future game or other projects.
